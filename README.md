# Christopher JdL portfolio website #

## What is this repository for ? 
This is the repository used to host the code of my showcase website, and the website itself. :rooster:

To access it [click here](http://christopherjdl.azurewebsites.net/) :smirk:

## Special Thanks
[StartBootstrap.com](http://StartBootstrap.com) : For their gorgeous theme.

TODO:

* Dates of all projects
* C++, python