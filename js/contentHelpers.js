/* Written by Christopher Jamme de Lagoutine */

function appyThisYearsDateToDocument() {
    var thisYear = new Date().getUTCFullYear();
    var thisYearElement = document.getElementById("thisYear");
    thisYearElement.appendChild(document.createTextNode(thisYear.toString()));
}
appyThisYearsDateToDocument();